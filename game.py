"""
File: game.py
Purpuse: Main module
Tab Width: 4
Revision: $Id$
"""

### Imports ###
import pygame
from OpenGL import GL
import time
import random
import math

import container
import steams
import mouse
import sprite
import train
import compare
import Options

### Globals ###
g_frameCounter		= 0
g_frameCounterTime	= 0

g_currentLevel = None

### class EndGameException ###
class EndGameException(Exception):
	pass

### class LevelConfig ###
class LevelConfig(object):
	def __init__(self):
		self.__dict__ = eval(file(Options.DefaultConfigFile, 'r').read())
	
	def loadFile(self, filename):
		self.__dict__.update(eval(file(filename, 'r').read()))

def getGameScreens():
	import welcome
	import level
	
	yield welcome.WelcomeScreen()
	for line in file(Options.LevelListFile,"r").readlines():
		line = line.strip()
		if line != "":
			yield level.GameLevel(line)

### class GameScreen ###
class GameScreen(object):	
	def __init__(self):
		global g_frameCounter,g_frameCounterTime
		self.screenActive 	= True
		self.mousePushState = False
		self.sprites		= container.SpriteContainer(self)
		self.steam			= steams.SteamContainer(self)
		self.config			= LevelConfig()
		self.highScore		= 0
		
		self.MouseSteamsToCreate = 0
	
	def start(self):
		self.lastTime		= time.time()
		g_frameCounter		= 0
		g_frameCounterTime	= 0
	
	def handleEvents(self, events):
		for event in events:
			if event.type == pygame.QUIT:
				raise EndGameException()
			
			elif event.type == pygame.MOUSEMOTION:
				self.onMouseMove(*event.pos)
				
			elif event.type == pygame.MOUSEBUTTONUP:
				self.onMouseClick(event.pos[0], event.pos[1], event.button, False)
			elif event.type == pygame.MOUSEBUTTONDOWN:
				self.onMouseClick(event.pos[0], event.pos[1], event.button, True)
			elif event.type == pygame.KEYDOWN:
				keyAscii = ''
				if event.key <= 255:
					keyAscii = chr(event.key)
				self.onKeyDown(event, keyAscii)
			elif event.type == pygame.VIDEORESIZE:
				self.onScreenResize(event.size)
			elif event.type == pygame.VIDEOEXPOSE:
				self.onVideoExpose()
	
	def doFrame(self,timeDelta):
		self.steam.doFrame(timeDelta)
		if self.mousePushState:
			self.steam.pushParticles(timeDelta, mouse.getPosition(), mouse.getDirection())
			self.MouseSteamsToCreate += timeDelta*Options.MouseSteamRate
			self.steam.createSteamBurst(int(self.MouseSteamsToCreate),
										mouse.getPosition(),
										mouse.getDirection(),
										velocity = lambda:random.random()*70+100,
										lifetime = Options.MouseSteamTime,
										color = Options.MouseSteamColor)
			self.MouseSteamsToCreate = math.modf(self.MouseSteamsToCreate)[0]
		self.sprites.doFrame(timeDelta)
	
	def draw(self):
		GL.glLoadIdentity()
		GL.glTranslated(-1,1,0)
		GL.glScalef(2.0/Options.WindowWidth,-2.0/Options.WindowHeight,1)
		
		GL.glEnable(GL.GL_BLEND)
		
		#Game Drawing
		self.sprites.drawVisible()
		self.steam.draw()
		mouse.DrawMouse()
	
	def isActive(self):
		return self.screenActive
	
	def onMouseClick(self, posX, posY, button, state):
		if button == 1:
			self.mousePushState = state
		self.sprites.callMethod('onMouseClick', posX, posY, button, state)

	def onMouseMove(self, newX, newY):
		mouse.MouseMove(newX, newY)
	
	def onKeyDown(self, event, keyAscii):
		#if keyAscii.lower() == 'q':
		#	raise EndGameException()
		if event.key == pygame.constants.K_ESCAPE:
			raise EndGameException()
	
	def onVideoExpose(self):
		#calculate the frame time
		curTime = time.time()
		timeDelta = curTime - self.lastTime
		self.lastTime = curTime
	
		#calculate FPS
		global g_frameCounter, g_frameCounterTime
		g_frameCounter += 1
		g_frameCounterTime += timeDelta
		if g_frameCounterTime > 1:
			g_frameCounterTime -= 1
			print "FPS: %3d ; Particle count: %d\t\t\r" % (g_frameCounter, len(self.steam)),
			g_frameCounter = 0
		
		self.doFrame(timeDelta)
	
		#clear the colors
		GL.glClear(GL.GL_COLOR_BUFFER_BIT)
		self.draw()
	
		GL.glFlush()
		pygame.display.flip()
	
	def onScreenResize(self, newSize):
		sprite.Sprite.callMethodSprites('onScreenResize', ((Options.WindowWidth, Options.WindowHeight), newSize))
		Options.WindowWidth, Options.WindowHeight = newSize
