"""
File: steams.py
Purpuse: All stuff related to drawing and manipulating steam
Tab Width: 4
Revision: $Id$
"""

### Imports ###
import container
import math
import pygame
import random
import time
from OpenGL import GL,GLU

import Options
import sprite
import game

### Globals ###
steam_tex_name = None
particle_display_list = None

### class Particle ##
class Particle:
	def __init__(self, screen, color, position = None, speed = [0.0,0.0], temperature = 0.5, targetPosition = None, lifeTime = None):
		global g_particles
		self.speed = list(speed)
		self.temperature = temperature
		self.timeLeft = lifeTime
		self.screen = screen
		if not position:
			position =  [150*(random.random()-0.5),150.0*(random.random()-0.5)]
			position[0] += Options.WindowWidth/2
			position[1] += Options.WindowHeight/2
		else:
			position = list(position)
		position[0] += (random.random()-0.5) * self.screen.config.SteamPositionRandomness
		position[1] += (random.random()-0.5) * self.screen.config.SteamPositionRandomness
		self.position = position
		self.targetPosition = targetPosition
		self.offset = [math.pi*random.random(),math.pi*random.random()]
		self.size = random.random()*12.0+13.0
		self.color = color
	
	def _cleanup(self):
		if (self.position[0] < 0 or self.position[0] > Options.WindowWidth or
			self.position[1] < 0 or self.position[1] > Options.WindowHeight):
			game.g_currentLevel.steam.remove(self)
	
	def _doFrame(self,TimePassed,factor):
		#reduce the TTL
		if None != self.timeLeft:
			self.timeLeft -= TimePassed
			if self.timeLeft<=0:
				game.g_currentLevel.steam.remove(self)
				return
		
		#change the offset
		self.offset[0] += 50 * self.temperature * (random.random()) * TimePassed
		self.offset[1] += 50 * self.temperature * (random.random()) * TimePassed
		
		self.position[0] += self.speed[0]*TimePassed
		self.position[1] += self.speed[1]*TimePassed
		
		self.speed[0] *= factor
		self.speed[1] *= factor
		
		#update the speed according to the target position
		if self.targetPosition:
			self.targetSpeed = [0,0]
			self.targetSpeed[0] = (self.targetPosition[0] - self.position[0])*0.5
			self.targetSpeed[1] = (self.targetPosition[1] - self.position[1])*0.5
			self.speed[0] = factor * self.speed[0] + (1-factor) * self.targetSpeed[0]
			self.speed[1] = factor * self.speed[1] + (1-factor) * self.targetSpeed[1]
	
	def draw(self):
		global particle_display_list
		GL.glColor4f(*self.color)
		
		GL.glPushMatrix()
		GL.glTranslatef(self.position[0] + math.sin(self.offset[0])*self.screen.config.OffsetFactor,
						self.position[1] + math.sin(self.offset[1])*self.screen.config.OffsetFactor,0)
		GL.glScalef(self.size,self.size,1)
		
		GL.glCallList(particle_display_list)
		
		GL.glPopMatrix()
	
	def push(self, TimePassed, direction, force):
		self.speed[0] += TimePassed * direction[0] * force
		self.speed[1] += TimePassed * direction[1] * force

### class SteamContainer ##
class SteamContainer(container.ObjectContainer):
	def __init__(self, screen):
		super(SteamContainer, self).__init__()
		self.screen = screen
		self.timer = 0
		
	def createRandomSteam(self, color = (0.0,0.8,0.1,0.2)):
		for i in xrange(Options.ParticleCount):
			self += Particle(self.screen, color)
	
	def createSteamBurst(self, amount, position, direction, velocity = lambda:1, lifetime = None, color = None):
		"""
		Usage:
		CreateSteamBurst(timePassed*100, current_train_position, rotate(train_angle,(0,-30)))
		CreateSteamBurst(timePassed*20, mouse_point, mouse_point-mouse_base, 1.2, Options.MouseSteamColor)
		"""
		
		if color == None:
			color = Options.colors[int(random.random()*len(Options.colors))] #change to random
		else:
			color = Options.colors[color]
		
		angle = math.atan2(direction[0],direction[1])
		angle += (random.random()-0.5) * self.screen.config.PushAngle
		direction = (math.sin(angle),math.cos(angle))
		
		vel = velocity()
		speed = [vel*direction[0], vel*direction[1]]
		
		if lifetime == None:
			for i in xrange(int(math.ceil(amount))):
				self += Particle(self.screen, color, list(position), speed, temperature = 0.8)
		else:
			for i in xrange(int(math.ceil(amount))):
				self += Particle(self.screen, color, list(position), speed, temperature = 0.8, lifeTime=lifetime)

	def doFrame(self, timePassed):
		while timePassed > self.screen.config.MinimalFrameTime:
			self.doFrame(self.screen.config.MinimalFrameTime)
			timePassed -= self.screen.config.MinimalFrameTime
		factor = math.pow(0.5,timePassed)
		self.callMethod('_doFrame', timePassed, factor)
		
		self.timer += timePassed
		if self.timer >= 5.0:
			self.timer = 0
			self.callMethod('_cleanup')
	
	def draw(self):
		global steam_tex_name
		
		#bind the texture
		GL.glEnable(GL.GL_TEXTURE_2D)
		GL.glBindTexture(GL.GL_TEXTURE_2D, steam_tex_name)
		GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
		GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST_MIPMAP_NEAREST)
		
		GL.glTexEnvi(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_MODULATE)
		
		#set color params
		GL.glEnable(GL.GL_BLEND)
		GL.glBlendFunc(GL.GL_SRC_ALPHA,GL.GL_ONE_MINUS_SRC_ALPHA)
		
		self.callMethod('draw')
		
		#restore color params
		GL.glDisable(GL.GL_BLEND)
		
		#unbind the texture
		GL.glDisable(GL.GL_TEXTURE_2D)
		GL.glBindTexture(GL.GL_TEXTURE_2D,0)
		GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)

	def getCenterOfMass(self):
		if len(self) == 0:
			return (0, 0)
		
		sum_position = [0,0]
		sum_size = 0
		
		for p in self:
			sum_position[0] += p.size * p.position[0]
			sum_position[1] += p.size * p.position[1]
			sum_size += p.size
			
		return (sum_position[0]/sum_size, sum_position[1]/sum_size)
	
	def loadSteam(self, image, factor = 1.0, steamOrigin = None, xRange = None):
		if not xRange:
			xRange = (0, image.get_width())
		for x in xrange(*xRange):
			if (x < 0) or (x >= image.get_width()):
				continue
			for y in xrange(0, image.get_height()):
				color = image.get_at((x, y))
				if color[3] > 0:
					# Convert to gl-like color
					color = map(lambda x: x/256.0, color)
					# Set alpha channel
					color[3] *= 0.4
					# Create particle
					self += Particle(self.screen, color, position = steamOrigin, targetPosition = [factor * x, factor * y])
	
	def pushParticles(self, TimePassed, MousePosition, MouseDirection):
		for p in self:
			diff = (p.position[0] - MousePosition[0], p.position[1] - MousePosition[1])
			diffLen2 = diff[0]*diff[0] + diff[1]*diff[1]
			if diffLen2 < self.screen.config.PushRadius*self.screen.config.PushRadius:
				diffLen = math.sqrt(diffLen2)
				diff = (diff[0]/diffLen,diff[1]/diffLen)
				CosAngle = diff[0]*MouseDirection[0] + diff[1]*MouseDirection[1]
				if CosAngle > self.screen.config.PushAngle:
					force = CosAngle * self.screen.config.PushForce * (1 - diffLen/self.screen.config.PushRadius)
					p.push(TimePassed, diff, force)

### Functions ###
def InitSteam():
	global steam_tex_name
	global particle_display_list
	
	#create the image, assuming its size is valid
	steam_image = pygame.image.load(Options.SteamImageFilename)
	tex_size = steam_image.get_size()

	#create the opengl texture object
	steam_tex_name = GL.glGenTextures(1)
	GL.glBindTexture(GL.GL_TEXTURE_2D,steam_tex_name)
	
	#copy the image to the opengl texture
	string = pygame.image.tostring(steam_image,"RGBA")
	GLU.gluBuild2DMipmaps(GL.GL_TEXTURE_2D, GL.GL_RGBA,
		tex_size[0],tex_size[1],
		GL.GL_RGBA,GL.GL_UNSIGNED_BYTE,string)

	#clean
	GL.glBindTexture(GL.GL_TEXTURE_2D,0)
	
	#create the display list
	particle_display_list = GL.glGenLists(1)
	GL.glNewList(particle_display_list,GL.GL_COMPILE)
	GL.glBegin(GL.GL_QUADS)
	for coord in [(0,0),(0,1),(1,1),(1,0)]:
		GL.glTexCoord2f(*coord)
		GL.glVertex2f(*coord)
	GL.glEnd()
	GL.glEndList()
