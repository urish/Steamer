"""
File: welcome.py
Purpuse: Welcome Screen related stuff
Tab Width: 4
Revision: $Id$
"""

### Imports ###
import game
import Options
import pygame
import sprite
import steams
import train

### Class WelcomeTrain ###
class WelcomeTrain(train.Train):
	def __init__(self, screen, imageFile, trainPathFile, speed):
		super(WelcomeTrain, self).__init__(screen, imageFile, trainPathFile, speed)
		self.steamingPoints = []
		self.welcomeCurrentPosition = 0
		self.welcomeImage = pygame.image.load("Data/images/welcome.png")
		self.welcomeImageScale = 8
		
	def doFrame(self, timePassed):
		super(WelcomeTrain, self).doFrame(timePassed)
		scaledPositionX = int(self.positionX / self.welcomeImageScale * 2)
		steamRange	= (self.welcomeCurrentPosition, scaledPositionX)
		steamerX	= int(self.positionX + self.steamerX)
		steamerY	= int(self.position[1] + self.steamerY)
		self.screen.steam.loadSteam(self.welcomeImage, self.welcomeImageScale, (steamerX, steamerY), steamRange)
		self.welcomeCurrentPosition = scaledPositionX

### class WelcomeScreen ###
class WelcomeScreen(game.GameScreen):
	def __init__(self):
		super(WelcomeScreen, self).__init__()
		self.sprites += sprite.BlinkingSprite("Data/images/startMessage.png", visible=True, position=(200,550))
		self.sprites += WelcomeTrain(self, 'Data/images/techno/locomotive.png', 'Data/images/techno/trainpath1.png', 75)
	
	def onKeyDown(self, event, keyAscii):
		if keyAscii == ' ':
			self.screenActive = False
			return
		super(WelcomeScreen, self).onKeyDown(event, keyAscii)

