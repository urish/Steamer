"""
File: sound.py
Purpuse: Game sound effects
Tab Width: 4
Revision: $Id$
"""

### Imports ###
import pygame

### Globals ###
g_hasSound = False
g_mixerChannel = None

### class SilentSoundEffect ###
class SilentSoundEffect(object):
	def __init__(self):
		pass
	
	def play(self):
		pass
	
	def playInfinite(self):
		pass
	
	def isPlaying(self):
		return False

### class SoundEffect ###
class SoundEffect(object):
	__soundEffects = {}
	
	def __init__(self, filename):
		self.__soundEffects[filename] = self
		self.sound = pygame.mixer.Sound(filename)
	
	def play(self, repeat = 1):
		global g_mixerChannel
		g_mixerChannel.play(self.sound)
	
	def playInfinite(self):
		self.play(-1)
	
	def isPlaying(self):
		pass
	
	def getSoundEffect(filename):
		global g_hasSound
		if not g_hasSound:
			return SilentSoundEffect()
		if filename in SoundEffect.__soundEffects:
			return SoundEffect.__soundEffects[filename]
		return SoundEffect(filename)
	getSoundEffect = staticmethod(getSoundEffect)
	
### Functions ###
def initSound():
	global g_hasSound
	global g_mixerChannel
	try:
		pygame.mixer.init()
	except:
		return False
	g_hasSound = True
	g_mixerChannel = pygame.mixer.Channel(0)
	return True
