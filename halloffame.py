"""
File: halloffame.py
Purpuse: HallOfFame screen related stuff
Tab Width: 4
Revision: $Id$
"""

### Imports ###
import game
import Options
import pygame
import sprite
import string

### class HallOfFameScreen ###
class HallOfFameScreen(game.GameScreen):
	def __init__(self, newScore):
		super(HallOfFameScreen, self).__init__()
		self.sprites += sprite.Sprite(Options.HallOfFameBackground, visible=True)
		self.newScore = None
		# Load hall of fame entries
		scoreData = eval(file(Options.ScoreFile, 'r').read())
		if newScore:
			self.newScore = ['', newScore]
			scoreData.append(self.newScore)
			scoreData.sort(lambda s1,s2: s2[1].__cmp__(s1[1]))
			scoreData = scoreData[:10]
			if self.newScore not in scoreData:
				self.newScore = None
		self.scoreData = scoreData
		basePosition = Options.HallOfFameBasePosition
		for index, (scoreName, scoreValue) in enumerate(scoreData):
			color = Options.HighScoreEntryColor
			if scoreData[index] == self.newScore:
				color = Options.HighScoreYourColor
			#scoreName = "%d. %s" % (index + 1, scoreName)
			position = [basePosition[0], basePosition[1] + index * Options.HallOfFameLineSpacing]
			nameSprite	= sprite.TextSprite(scoreName, color, bold=True, position=list(position))
			position[0]  += Options.HallOfFameNameWidth
			scoreSprite = sprite.TextSprite(str(scoreValue), color, bold=True, position=position)
			self.sprites += nameSprite
			self.sprites += scoreSprite
			if scoreData[index] == self.newScore:
				self.newScoreSprites = (nameSprite, scoreSprite)
	
	def onKeyDown(self, event, keyAscii):
		if self.newScore:
			if (len(event.unicode) == 1) and (event.unicode in string.printable) and ord(event.unicode) >= 32:
				self.newScore[0] += event.unicode
				self.newScoreSprites[0].setText(self.newScore[0])
				return
			
			if event.key in [pygame.constants.K_BACKSPACE, pygame.constants.K_LEFT]:
				if len(self.newScore[0]) > 0:
					self.newScore[0] = self.newScore[0][:-1]
					self.newScoreSprites[0].setText(self.newScore[0])
					return
			
			if event.key == pygame.constants.K_RETURN:
				self.newScoreSprites[0].setTextColor(Options.HighScoreEntryColor)
				self.newScoreSprites[1].setTextColor(Options.HighScoreEntryColor)
				self.newScore = None
				self.saveHighscore()
				return
		
		super(HallOfFameScreen, self).onKeyDown(event, keyAscii)

	def saveHighscore(self):
		scoreFile = file(Options.ScoreFile, 'w')
		scoreFile.write(repr(self.scoreData))
		scoreFile.close()

