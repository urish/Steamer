"""
File: container.py
Purpuse: ObjectContainer class
Tab Width: 4
Revision: $Id$
"""

### class ObjectContainer ###
class ObjectContainer(object):
	def __init__(self):
		self.__objects = []
		
	def __add__(self, object):
		self.__objects.append(object)
		return self
	
	def __iter__(self):
		return iter(self.__objects)
	
	def getList(self):
		return self.__objects
	
	def __len__(self):
		return len(self.__objects)
	
	def __sub__(self, object):
		self.__objects.remove(object)
		return self
	
	def add(self, object):
		self += object
	
	def remove(self, object):
		self -= object
		
	def callMethod(self, methodName, *arguments, **kwArguments):
		self.callMethodEx(methodName, arguments, kwArguments)
	
	def callMethodEx(self, methodName, arguments, kwArguments, filterFunc = None):
		for sprite in self:
			method = getattr(sprite, methodName)
			if not filterFunc or filterFunc(sprite):
				method(*arguments, **kwArguments)

### class SpriteContainer ###
class SpriteContainer(ObjectContainer):
	def __init__(self, screen = None):
		super(SpriteContainer, self).__init__()
		self.screen = screen
	
	def __add__(self, object):
		if self.screen:
			object.setScreen(self.screen)
		return super(SpriteContainer, self).__add__(object)
	
	def __sub__(self, object):
		if self.screen:
			object.setScreen(None)
		return super(SpriteContainer, self).__sub__(object)
		
	def drawVisible(self):
		self.callMethodEx('draw', (), {}, lambda sprite: sprite.isVisible())

	def doFrame(self, timePassed):
		self.callMethod('doFrame', timePassed)
