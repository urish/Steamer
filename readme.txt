#######################################################
######################## Steamer ######################
#######################################################

File Revision: $Id$

1. Requirments
#######################################################
* Python 2.4 (http://www.python.org/)
* PyGame version 1.7.1 (http://www.pygame.org/download.shtml)
* PyOpenGL version 2.0.2.01 (http://sourceforge.net/project/showfiles.php?group_id=5988)
* Numeric version 24.2 (http://sourceforge.net/project/showfiles.php?group_id=1369&package_id=1351)

And optionally psyco (http://psyco.sourceforge.net/), which will make the
program run faster. Due to memory leak in previous versions, version 1.5.1
or above is required.

2. Game Play
#######################################################
The main goal of the game is to make shapes out of steam.
The shape you need to create is shown at the top right corner of the screen.

Shaping is done with the mouse movement and left button. Note that the
direction of the mouse movement affects the direction of the mouse
cursor, whice in turn affects the direction of the steam. Try clicking
the left button of the mouse for a while, in front of some steam, and
you will get the gist.

To compare the shape with the steam click the right mouse button anywhere
to hint `Steamer` where the shape is on the screen, or hit the 'C' key to
compare using the Center of mass of the steam as a hint. After a second
or so, you will see the shape behind the steam, showing the best matching
position found. Anytime during the comparison, click again to cancel the
current comparison and restart.

The shape similarity bar shows how close you are to the shape (your score)
in red and how close you were in the last comparison with a white line.

Use space anytime to move to the next level, and add your level score to the
total game score.

3. Credits
#######################################################
Thanks to all who took part in this project:
Uri, Nir, Yuriyuriyuri & Liz.


Eel
