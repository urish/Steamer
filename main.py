"""
File: main.py
Purpuse: Main module of the game
Tab Width: 4
Revision: $Id$
"""

### Imports ###
import pygame
import game
import level
import mouse
import Options
import sound
import steams
import sys

### Code ###

def runGame(arguments):
	pygame.display.init()
	pygame.font.init()
		
	pygame.display.set_caption('Steamer Game')
	flags = pygame.DOUBLEBUF | pygame.OPENGL # | pygame.RESIZABLE | HWSURFACE
	if '-f' in arguments:
		flags |= pygame.FULLSCREEN
	window = pygame.display.set_mode((Options.WindowWidth,Options.WindowHeight),flags)
	screen = pygame.display.get_surface()
	
	# Initialize modules
	steams.InitSteam()
	mouse.InitMouse()
	sound.initSound()
	
	def doLevel(level):
		game.g_currentLevel = currentLevel
		currentLevel.start()
		while currentLevel.isActive():
			events = pygame.event.get()
			if [] == events:
				pygame.event.post(pygame.event.Event(pygame.VIDEOEXPOSE,{}))
			else:
				currentLevel.handleEvents(events)
	
	try:
		Score = 0
		for currentLevel in game.getGameScreens():
			doLevel(currentLevel)
			Score += currentLevel.highScore
		import halloffame
		currentLevel = halloffame.HallOfFameScreen(Score)
		doLevel(currentLevel)
	except game.EndGameException:
		pass
	
	pygame.quit()

if __name__ == '__main__':
	# Psyco acceleration
	try:
		import psyco
		assert psyco.__version__ >= 17105392
		psyco.profile(memory = 30000) #use maximum of 30 megs of memory for psyco data structures
	except ImportError:
		print "Install psyco 1.5.1 for best performance"

	runGame(sys.argv)
