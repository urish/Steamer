"""
File: compare.py
Purpuse: Shape Comparison Algorithm
Tab Width: 4
Revision: $Id$
"""

from OpenGL import GL
import time

import steams
import Options

class ShapeInfo:
	def __init__(self, image, factor):
		self.size = image.get_size()
		
		self.pointList = []
		self.outOfBoundsList = []
		
		for x in xrange(0, image.get_width()):
			for y in xrange(0, image.get_height()):
				color = image.get_at((x, y))
				color = color[0:3]
				if color == (0,0,255):
					self.pointList.append((factor*x, factor*y))
				elif color == (255,0,0):
					self.outOfBoundsList.append((factor*x, factor*y))
				elif color == (255,255,255) or color == (0,0,0):
					pass
				else:
					raise Exception("Invalid shape image at %d,%d" % (x,y))
	def getPointList(self):
		return self.pointList
	def getOutOfBoundList(self):
		return self.outOfBoundsList

class CompareConfig:
	def __init__(self, Scale, Position, StepSize):
		self.Scale = Scale
		self.Position = Position
		self.StepSize = StepSize
	
	#the parameters RelativeScale and offset are used for both position and negative versions
	def ChangeScale(self, RelativeScale):
		return CompareConfig(self.Scale+RelativeScale/100.0, self.Position, self.StepSize) #note the + and not *
	def MoveHorizontal(self, offset):
		return CompareConfig(self.Scale, (self.Position[0]+offset,self.Position[1]), self.StepSize)
	def MoveVertical(self, offset):
		return CompareConfig(self.Scale, (self.Position[0],self.Position[1]+offset),self.StepSize)
	
	def ApplyTo(self, x,y):
		x *= self.Scale
		y *= self.Scale
		x += self.Position[0]
		y += self.Position[1]
		return (x,y)

class ParticleMap:
	def __init__(self, particles):		
		#prepare matrix for rendering
		## Note that this is different from the other matrix initializations, because we want to make it "upside-down"
		GL.glPushMatrix()
		GL.glLoadIdentity()
		GL.glTranslated(-1,-1,0)
		GL.glScalef(2.0/Options.WindowWidth,2.0/Options.WindowHeight,1)
		
		#render the particles to the back screen buffer using opengl
		GL.glDisable(GL.GL_BLEND)
		GL.glDisable(GL.GL_TEXTURE_2D)
		GL.glClear(GL.GL_COLOR_BUFFER_BIT)
		particles.callMethod('draw')
		GL.glPopMatrix()
		
		#copy the image from the back screen buffer to a texture
		GL.glFlush()
	
	def checkPosition(self, compareConfig, x, y):
		x,y = map(int,compareConfig.ApplyTo(x,y))
		if x < 0 or x >= Options.WindowWidth:
			return 0
		if y < 0 or y >= Options.WindowHeight:
			return 0
		pixel = GL.glReadPixels(x,y, 1,1,GL.GL_LUMINANCE, GL.GL_UNSIGNED_BYTE)[0]
		return ord(pixel)

def _Compare(shapeInfo, compareConfig, steam):
	#Create a particle map (render of the onscreen particles)
	map = ParticleMap(steam)
	
	score = 0
	for (x,y) in shapeInfo.getPointList():
		if map.checkPosition(compareConfig, x, y) > 0:
			score += 1
	if score == 0: #still
		return None
	for (x,y) in shapeInfo.getOutOfBoundList():
		if map.checkPosition(compareConfig, x, y) > 0:
			score -= 1
	return score

def createInitialCompareQueue(startPosition,imageSize, factor):
	#initializing the initial shape config
	initialConfigPosition = startPosition
	initialConfigPosition[0] -= imageSize[0] * factor / 2
	initialConfigPosition[1] -= imageSize[1] * factor / 2
	initialConfig = CompareConfig(1.0, initialConfigPosition, 8.0)#power of two, preferrably
	return initialConfig

def getConfigVariation(config):
	retval = []
	retval += [config.MoveHorizontal(config.StepSize),config.MoveHorizontal(-config.StepSize)]
	retval += [config.MoveVertical(config.StepSize),config.MoveVertical(-config.StepSize)]
	retval += [config.ChangeScale(config.StepSize)]
	if config.Scale > 0.75:
		retval += [config.ChangeScale(-config.StepSize)]
	return retval

class CompareStatus:
	def __init__(self, shapeInfo):
		self.compareQueue = []
		self.bestConfig = None
		self.bestScore = None
		self.shapeInfo = shapeInfo
	
	def beginCompare(self, startPosition):
		self.compareQueue = [createInitialCompareQueue(startPosition,self.shapeInfo.size,4.0)]
		self.bestConfig = None
		self.bestScore = None

# # # exported functions: # # #
	
def compareOne(compareStatus,steam):
	if compareStatus.compareQueue == None or compareStatus.compareQueue == []:
		return None
	
	#pop a config and score it
	testConfig = compareStatus.compareQueue.pop()
	testScore = _Compare(compareStatus.shapeInfo,testConfig,steam)
	
	#test if we found the best config (so far)
	if testScore != None:
		if compareStatus.bestScore == None or testScore > compareStatus.bestScore:
			compareStatus.bestScore = testScore
			compareStatus.bestConfig = testConfig
			compareStatus.compareQueue = getConfigVariation(testConfig) + compareStatus.compareQueue
	
	del testConfig,testScore
	
	#if done comparing for this stepsize, try to continue comparing using the best config and smaller stepsize
	if compareStatus.compareQueue == []:
		if compareStatus.bestConfig != None:
			if compareStatus.bestConfig.StepSize>2:
				compareStatus.bestConfig.StepSize *= 0.5
				compareStatus.compareQueue = getConfigVariation(compareStatus.bestConfig)
	
	#if the final last compare has been done. return  an answer
	if compareStatus.compareQueue == []:
		if compareStatus.bestScore != None:
			return compareStatus.bestScore,compareStatus.bestConfig
		else:
			return 0
	
	#there are still things to compare
	return None
