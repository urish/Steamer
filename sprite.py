"""
File: sprite.py
Purpuse: Sprite class
Tab Width: 4
Revision: $Id$
"""

### Imports ###
import pygame
from OpenGL import GL,GLU
import Options

### class SpriteImage ###
class SpriteImage(object):
	__allocatedImages = {}
	
	def __init__(self, orig_image, use_mipmapping = False):
		def Make2n(number):
			i = 1
			while i < number:
				i*=2
			return i
		
		self.mipMapped = use_mipmapping
		
		#create an image with OpenGL-compatible size
		if not hasattr(orig_image, 'blit'):
			orig_image = pygame.image.load(orig_image)
		self.size = orig_image.get_size()
		self.tex_size = tuple(map(Make2n,self.size))
		tex_image = pygame.Surface(self.tex_size,pygame.SRCALPHA,32)
		tex_image.blit(orig_image,(0,0))
		del orig_image
		
		#create the opengl texture object
		self.tex_name = GL.glGenTextures(1)
		GL.glBindTexture(GL.GL_TEXTURE_2D,self.tex_name)
		
		#copy the image to the opengl texture
		string = pygame.image.tostring(tex_image,"RGBA")
		if use_mipmapping:
			GLU.gluBuild2DMipmaps(GL.GL_TEXTURE_2D, GL.GL_RGBA,
				self.tex_size[0],self.tex_size[1],
				GL.GL_RGBA,GL.GL_UNSIGNED_BYTE,string)
		else:
			GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
				self.tex_size[0],self.tex_size[1],0,
				GL.GL_RGBA,GL.GL_UNSIGNED_BYTE,string)
		
		#clean
		GL.glBindTexture(GL.GL_TEXTURE_2D,0)
		
	def getSize(self):
		return self.size
	
	def getSpriteImage(imageFile):
		if imageFile not in SpriteImage.__allocatedImages:
			SpriteImage.__allocatedImages[imageFile] = SpriteImage(imageFile)
		return SpriteImage.__allocatedImages[imageFile]
	getSpriteImage = staticmethod(getSpriteImage)

	def draw(self):
		"draws the image to (0,0) - (width,height)"
		
		#bind the texture
		GL.glEnable(GL.GL_TEXTURE_2D)
		GL.glBindTexture(GL.GL_TEXTURE_2D,self.tex_name)
		if not self.mipMapped:
			GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
			GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
		
		#prepare the matrices
		GL.glPushMatrix() #modelview
		GL.glScalef(self.size[0],self.size[1],1)
		
		GL.glMatrixMode(GL.GL_TEXTURE)
		GL.glPushMatrix()
		GL.glLoadIdentity()
		GL.glScalef(float(self.size[0])/self.tex_size[0], float(self.size[1])/self.tex_size[1], 1)
		
		#draw
		GL.glBegin(GL.GL_QUADS)
		for coord in [(0,0),(0,1),(1,1),(1,0)]:
			GL.glTexCoord2f(*coord)
			GL.glVertex2f(*coord)
		GL.glEnd()
		
		#restore the matrices
		GL.glMatrixMode(GL.GL_TEXTURE)
		GL.glPopMatrix()
		GL.glMatrixMode(GL.GL_MODELVIEW)
		GL.glPopMatrix()
		
		#unbind the texture
		GL.glDisable(GL.GL_TEXTURE_2D)
		GL.glBindTexture(GL.GL_TEXTURE_2D,0)
	
	def __del__(self):
		if GL.glDeleteTextures:
			GL.glDeleteTextures([self.tex_name])

### Class Sprite ###
class Sprite(object):
	def __init__(self, imageFile, position = (0,0), scale = (1,1), angle=0, blendingEnabled = False, visible = False, timeOut = None):
		self.image = None
		if imageFile:
			self.image = SpriteImage.getSpriteImage(imageFile)
		self.position = position
		self.scale = scale
		self.angle = angle #360
		self.__blendingEnabled = blendingEnabled
		self.__visible = visible
		self.timeOut = timeOut
		self.screen = None
		self.color = [1,1,1,1]
	
	def doFrame(self, timePassed):
		if None != self.timeOut:
			self.timeOut -= timePassed
			if self.timeOut <= 0:
				self.timeOut = None
				self.setVisible(False)
	
	def setTimeOut(self,timeOut):
		self.timeOut = timeOut
	
	def setColor(self, color):
		self.color = color
	
	def draw(self):
		if self.__blendingEnabled:
			GL.glEnable(GL.GL_BLEND)
			GL.glBlendFunc(GL.GL_SRC_ALPHA,GL.GL_ONE_MINUS_SRC_ALPHA)
		
		GL.glColor4f(*self.color)
		if self.color != [1,1,1,1]:
			GL.glTexEnvi(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_MODULATE)
		else:
			GL.glTexEnvi(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_REPLACE)
		
		GL.glPushMatrix()
		GL.glTranslatef(self.position[0],self.position[1],0)
		GL.glRotatef(self.angle,0,0,1)
		GL.glScalef(self.scale[0],self.scale[1],1)
		self.image.draw()
		GL.glPopMatrix()
		
		GL.glDisable(GL.GL_BLEND)
		
	def onMouseClick(self, posX, posY, button, state):
		pass

	def isVisible(self):
		return self.__visible
	
	def getHeight(self):
		return self.image.getSize()[1]
	
	def getPosition(self):
		return self.position
	
	def getScale(self):
		return self.scale
	
	def getScreen(self):
		return self.screen
	
	def getSize(self):
		return self.image.getSize()
	
	def getWidth(self):
		return self.image.getSize()[0]
	
	def contains(self, posX, posY):
		"WARNING: this function is not complete. it assumes that rotate is 0 and scale is either (1,1) or (1,-1)"
		size = self.image.getSize()
		if self.scale[0]>=0:
			if posX >= self.position[0] and posY >= self.position[1]:
				newX,newY = posX-self.position[0], posY-self.position[1]
				if newX < size[0] and newY < size[1]:
					return True
		else:
			if posX <= self.position[0] and posY >= self.position[1]:
				newX,newY = self.position[0]-posX, posY-self.position[1]
				if newX < size[0] and newY < size[1]:
					return True
		return False
	
	def onScreenResize(self, oldSize, newSize):
		self.position = (int(self.position[0] * newSize[0] / oldSize[0]), 
						 int(self.position[1] * newSize[1] / oldSize[1]))
		
	def setPosition(self, value):
		self.position = value
	
	def setScale(self, value):
		self.scale = value
	
	def setScreen(self, screen):
		self.screen = screen
	
	def setVisible(self, value = True):
		self.__visible = value

### Class BlinkingSprite ###
class BlinkingSprite(Sprite):
	def __init__(self, imageFile, position = (0,0), scale = (1,1), angle=0, blendingEnabled = False, 
	visible = False, timeOut = None, blinkInterval = 0.7):
		super(BlinkingSprite, self).__init__(imageFile, position, scale, angle, blendingEnabled, visible, timeOut)
		self.blinkInterval = blinkInterval
		self.countingToNextBlink = 0

	def doFrame(self, timePassed):
		super(BlinkingSprite, self).doFrame(timePassed) 
		if None != self.blinkInterval:
			self.countingToNextBlink += timePassed
			if self.countingToNextBlink >= self.blinkInterval:
				self.setVisible(not self.isVisible())
				self.countingToNextBlink = 0

### Class TextSprite ###
class TextSprite(Sprite):
	def __init__(self, text, textColor = (255,255,255), position = (0,0), visible = True, bold = False):
		super(TextSprite, self).__init__(None, position = position, blendingEnabled = True, visible = visible)
		# Render the given text
		self.font = pygame.font.SysFont('Arial', 16, bold)
		self.text = text
		self.textColor = textColor
		self.render()
		
	def getTextColor(self):
		return self.textColor
	
	def getText(self):
		return self.text
	
	def render(self):
		renderedText = self.font.render(self.text, True, self.textColor)
		self.image = SpriteImage(renderedText)
	
	def setTextColor(self, textColor):
		self.textColor = textColor
		self.render()
	
	def setText(self, text):
		self.text = text
		self.render()
