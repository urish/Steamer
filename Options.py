"""
File: Options.py
Purpuse: Many constants and stuff
Tab Width: 4
Revision: $Id$
"""

import random

#Game window:
WindowWidth = 800
WindowHeight = 600

#Gaming Options
LevelListFile	= "Data/levels/levellist.txt"
DefaultConfigFile	= "Data/levels/default.txt"
LevelConfigFile 	= "Data/levels/%s.txt"

ThemePathFormat		= "Data/images/%s/"

# Trains
TrainHornSound 		= r"Data/sound/TrainHorn.ogg"

# Steams & Particles
SteamImageFilename	= "Data/images/steam.png"
ParticleCount		= 500
colors = [(1,1,1,0.2),(1,0,1,0.2),(1,0,0,0.2),(0,0,1,0.2),(0,1,1,0.2),(0,1,0,0.2)]

#Mouse:
MouseSize		= 35
Cursor			= r"Data/images/cursor.png"
CursorCenter	= (16,0)
MouseSteamRate	= 35
MouseSteamTime	= 0.8
MouseSteamColor = 0

#Shapes:
shapeImage = r"Data/images/shapes/%s.png"
shapeInfo = r"Data/images/shapes/%s.bmp"

processingImageFilename = r"Data/images/processing.png"

# Hall of fame
HallOfFameBackground	= r'Data/images/HallOfFame.png'
HallOfFameBasePosition	= (128, 119)
HallOfFameLineSpacing	= 36
HallOfFameNameWidth		= 400
HighScoreEntryColor		= (150, 150, 255)
HighScoreYourColor		= (255, 150, 150)
ScoreFile				= 'Data/score.txt'
