"""
File: mouse.py
Purpuse: Mouse cursor that changes angle according to mouse movements
Tab Width: 4
Revision: $Id$
"""

import math
import pygame
from OpenGL import GL

import sprite
import Options

MouseBase = (0.0,0.0)
MousePoint = (0.0,1.0)
MouseSprite = None

def InitMouse():
	global MouseSprite
	pygame.mouse.set_visible(False)
	MouseSprite = sprite.SpriteImage(Options.Cursor,True)

def getPosition():
	return MousePoint

def getDirection():
	MouseDir = (MousePoint[0] - MouseBase[0], MousePoint[1] - MouseBase[1])
	#normalize
	MouseDirLen = math.sqrt(MouseDir[0]*MouseDir[0] + MouseDir[1]*MouseDir[1])
	MouseDir = (MouseDir[0] / MouseDirLen, MouseDir[1] / MouseDirLen)
	return MouseDir
	
def MouseMove(newX, newY):
	global MouseBase,MousePoint
	MousePoint = (1.0*newX, 1.0*newY)
	MouseDir = getDirection()
	MouseBase = (MousePoint[0] - Options.MouseSize*MouseDir[0], MousePoint[1] - Options.MouseSize*MouseDir[1])

def DrawMouse():
	global MouseBase,MousePoint
	global MouseSprite
	
	GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR_MIPMAP_LINEAR)
	
	GL.glEnable(GL.GL_BLEND)
	GL.glBlendFunc(GL.GL_SRC_ALPHA,GL.GL_ONE_MINUS_SRC_ALPHA)
	GL.glColor4f(1,1,1,1)
	
	GL.glPushMatrix()
	GL.glTranslatef(MousePoint[0],MousePoint[1],0)
	GL.glRotatef(180/3.1415*math.atan2(MousePoint[0]-MouseBase[0],MouseBase[1]-MousePoint[1]),0,0,1)
	GL.glScalef(0.5,0.5,1)
	GL.glTranslatef(-Options.CursorCenter[0],-Options.CursorCenter[1],0)
	MouseSprite.draw()
	GL.glPopMatrix()
	
	GL.glDisable(GL.GL_BLEND)
	
	if False:
		GL.glColor3f(0,0,0)
		GL.glPointSize(5)
		GL.glBegin(GL.GL_POINTS)
		GL.glVertex2f(MousePoint[0],MousePoint[1])
		GL.glEnd()
