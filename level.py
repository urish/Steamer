"""
File: level.py
Purpuse: Level Screen implementation and LevelConfig class
Tab Width: 4
Revision: $Id$
"""

### Imports ###
import game
import Options
import os
import pygame
import sprite
import train
import compare
import random
from OpenGL import GL

### class Track ###
class Track(object):
	def __init__(self, level, trackPathFile):
		self.level = level
		self.trackPathFile = trackPathFile
		
		self.createTrain()
		self.level.sprites += self.train
	
	def createTrain(self):
		speed = 50 + 20*random.random()
		if random.random()>0.5:
			speed *= -1
		self.train = train.Train(
			self.level,
			self.level.getThemeFile('locomotive.png'), 
			self.level.getThemeFile(self.trackPathFile),speed)
	
	def doFrame(self, timeDelta):
		if self.train.isVisible() == False:
			self.level.sprites -= self.train
			self.createTrain()
			self.level.sprites += self.train
	
	def __del__(self):
		self.level.sprites -= self.train

class ScoreMeter:
	def __init__(self, maxScore):
		self.highScore = 0
		self.currentScore = 0
		self.maxScore = maxScore
	
	def draw(self):
		GL.glPushMatrix()
		GL.glTranslatef(748,75,0)
		GL.glScalef(40,200,1)
		
		#high score
		s = float(self.highScore)/self.maxScore
		GL.glEnable(GL.GL_BLEND)
		GL.glColor4f(1,0,0,float(self.highScore)/self.maxScore)
		GL.glBegin(GL.GL_QUADS)
		GL.glVertex2f(0,1-s)
		GL.glVertex2f(0,1)
		GL.glVertex2f(1,1)
		GL.glVertex2f(1,1-s)
		GL.glEnd()
		
		#frame
		GL.glDisable(GL.GL_BLEND)
		GL.glColor3f(1,1,1)
		GL.glBegin(GL.GL_LINE_LOOP)
		GL.glVertex2f(0,0)
		GL.glVertex2f(0,1)
		GL.glVertex2f(1,1)
		GL.glVertex2f(1,0)
		GL.glEnd()
		
		#currentScore
		s = float(self.currentScore)/self.maxScore
		GL.glBegin(GL.GL_LINES)
		GL.glVertex2f(-0.2,1-s)
		GL.glVertex2f(1.2,1-s)
		GL.glEnd()
		
		GL.glPopMatrix()
	
	def updateScore(self, score):
		self.currentScore = score
		if score > self.highScore:
			self.highScore = score

### class GameLevel ###
class GameLevel(game.GameScreen):
	def __init__(self, levelNumber):
		super(GameLevel, self).__init__()
		
		# Load configuration
		self.config.loadFile(Options.LevelConfigFile % (levelNumber, ))
		self.themePath = Options.ThemePathFormat % (self.config.Theme, )
		
		#Background
		self.sprites += sprite.Sprite(self.getThemeFile('gamebg.png'), visible = True)
		
		#prepare the compare and shape information
		shapeInfoImage = pygame.image.load(Options.shapeInfo % self.config.Shape)
		shapeInfo = compare.ShapeInfo(shapeInfoImage,4.0)
		self.compareStatus = compare.CompareStatus(shapeInfo)
		
		self.processingSprite = sprite.Sprite(Options.processingImageFilename, blendingEnabled=True)
		self.processingSprite.setPosition((0, Options.WindowHeight - self.processingSprite.getHeight()))
		self.processingSprite.setVisible(False)
		self.sprites += self.processingSprite
		
		#Score Meter
		self.scoremeter = ScoreMeter(len( shapeInfo.getPointList() ))
		
		# Create the shape panel
		shapeImageFile = Options.shapeImage % self.config.Shape
		shapePanel = sprite.Sprite(shapeImageFile, visible = True, blendingEnabled=True)
		shapePanel.setPosition((Options.WindowWidth - shapePanel.getWidth(), 0))
		self.sprites += shapePanel
		
		#shapeGhost
		self.shapeGhost = sprite.Sprite(shapeImageFile,blendingEnabled=True)
		self.sprites += self.shapeGhost
		
		#train tracks
		self.tracks = []
		for line in file(self.getThemeFile('tracks.txt'),"r").readlines():
			line = line.strip()
			if line != None:
				self.tracks.append(Track(self,line))
	
	def doFrame(self,timeDelta):
		super(GameLevel, self).doFrame(timeDelta)
		self.compareImages()
		for t in self.tracks:
			t.doFrame(timeDelta)
	
	def draw(self):
		super(GameLevel, self).draw()
		self.scoremeter.draw()
	
	def getThemeFile(self, fileName):
		return os.path.join(self.themePath, fileName)
	
	def startComparing(self, position):
		self.processingSprite.setVisible(True)
		self.compareStatus.beginCompare(position)
		
	def compareImages(self):
		CompareResult = compare.compareOne(self.compareStatus, self.steam)
		if None != CompareResult: # the comparison has finished
			self.processingSprite.setVisible(False)
			if CompareResult == 0:
				self.scoremeter.updateScore(0)
				self.shapeGhost.setVisible(False)
			else:
				score, shapePosition = CompareResult
				
				self.scoremeter.updateScore(score)
				
				self.shapeGhost.setPosition(shapePosition.Position)
				self.shapeGhost.setScale((shapePosition.Scale*4,shapePosition.Scale*4))
				#self.shapeGhost.setTimeOut(1.0)
				self.shapeGhost.setColor((1,1,1,0.3))
				self.shapeGhost.setVisible(True)
				if score > self.highScore:
					self.highScore = score				
		
	def onKeyDown(self, event, keyAscii):
		if keyAscii.lower() == 'c':
			self.startComparing(list(self.steam.getCenterOfMass()))
			return
		if keyAscii.lower() == 'q' or keyAscii.lower() == ' ':
			self.screenActive = False
		super(GameLevel, self).onKeyDown(event, keyAscii)
	
	def onMouseClick(self, newX, newY, button, state):
		super(GameLevel, self).onMouseClick(newX, newY, button, state)
		if button == 3 and state == True:
			self.startComparing([newX,newY])
