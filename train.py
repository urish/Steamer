"""
File: train.py
Purpuse: Train class
Tab Width: 4
Revision: $Id$
"""

### Imports ###
import math
import Options
import pygame
import random
import sprite
import steams
import sound
import os

### Class Train ###
class Train(sprite.Sprite):
	def __init__(self, screen, imageFile, trainPathFile, speed):
		super(Train, self).__init__(imageFile, blendingEnabled = True)
		
		# Initialize train parameters
		self.speed = speed
		self.positionX = 0
		self.radianAngle = 0.0
		self.steaming = False
		self.path = None
		self.setVisible(True)
		self.screen = screen
		
		self.inSteamingPoint = False
		
		# Initialize horn sound
		self.hornSound = sound.SoundEffect.getSoundEffect(Options.TrainHornSound)
		
		# Load config variables
		configFile = os.path.splitext(imageFile)[0] + '.txt'
		config = eval(file(configFile, 'r').read())
		self.__dict__.update(config)
		
		self.loadTrainPath(trainPathFile)
		
		# Update train position
		if self.speed >= 0:
			self.positionX = -1.0 * self.getWidth()
		else:
			self.positionX = len(self.path) + self.getWidth()
			self.setScale((-1,1))
			
		self.position = (self.positionX, self.getPathPositionY(self.positionX))

	def getPathPositionY(self, positionX):
		if positionX < 0:
			return self.path[0]
		if positionX >= len(self.path):
			return self.path[-1]
		return self.path[positionX]
	
	def onMouseClick(self, posX, posY, button, state):
		if state == False:
			self.inSteamingPoint = False
		else:
			if self.contains(posX,posY):
				self.inSteamingPoint = True
				self.hornSound.play()
	
	def doFrame(self, timePassed):
		self.positionX += timePassed * self.speed * (1 + math.sin(self.radianAngle))
		
		if self.positionX < -self.getWidth() or self.positionX > len(self.path) + self.getWidth():
			self.setVisible(False)
		
		# Find normalized position in range [0,1)
		normalizedPosition = self.positionX / len(self.path)
		
		# Find wheel positions
		backWheelX	= int(self.positionX + self.backWheelX)
		backWheelY  = self.getPathPositionY(backWheelX)
		frontWheelX	= int(self.positionX +  self.frontWheelX)
		frontWheelY  = self.getPathPositionY(frontWheelX)
		
		# Calculate rotation angle
		self.radianAngle = math.atan(float(frontWheelY - backWheelY) / (frontWheelX - backWheelX))
		self.angle = self.radianAngle / math.pi * 180
		
		# Calculate train position
		backWheelPointAngle		= math.atan(self.wheelsY / self.backWheelX)
		backWheelPointRadius	= math.sqrt(self.backWheelX ** 2 + self.wheelsY ** 2)
		backWheelNewAngle		= backWheelPointAngle - self.radianAngle
		backWheelNewX			= math.cos(backWheelNewAngle) * backWheelPointRadius
		backWheelNewY			= math.sin(backWheelNewAngle) * backWheelPointRadius
		positionX		= self.positionX + backWheelNewX - self.backWheelX
		positionY		= backWheelY - self.wheelsY + backWheelNewY - self.wheelsY
		self.position	= (positionX, positionY)
		
		# Steam handling
		sign		= lambda n: 1 - 2 * int(n < 0)
		steamerX	= int(self.positionX) + sign(self.speed) * self.steamerX
		steamerY	= int(self.position[1] + self.steamerY)

		if self.inSteamingPoint:
			self.screen.steam.createSteamBurst(timePassed * self.screen.config.TrainSteamFactor, 
											  (steamerX, steamerY), 
											  self.screen.config.TrainSteamVelocity,
											  velocity = lambda:50*random.random()+30,
											  color = self.steamColor)
	
	def loadTrainPath(self, filename):
		cacheFilename = os.path.splitext(filename)[0] + '.txt'
		if os.path.exists(cacheFilename):
			self.path = eval(file(cacheFilename, 'r').read())
			return
		
		image = pygame.image.load(filename)
		
		self.path = []
		
		avg = lambda lst: sum(lst) / len(lst)
		
		for x in range(0, image.get_width()):
			pixels = [y for y in range(0, image.get_height()) if image.get_at((x, y))[3] > 0]
			if len(pixels) > 0:
				self.path += [avg(pixels)]
			else:
				self.path += [0]
		
		del image
		try:
			file(cacheFilename, 'w').write(repr(self.path))
		except OSError:
			pass
